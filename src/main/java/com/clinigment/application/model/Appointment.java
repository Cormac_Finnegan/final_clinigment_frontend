
package com.clinigment.application.model;

import com.clinigment.model.adapters.LocalDateAdapter;
import com.clinigment.model.adapters.LocalDateTimeAdapter;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;



/**
 *
 * @author csaba
 */


@XmlRootElement
public class Appointment implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @XmlID
    private Long id;
    
    
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate date;
    
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private LocalDateTime startTime;
    
    
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    
    private LocalDateTime endTime;
    
    
    private Long patientId;
    
    
    private Patient patient;
    
    
    private String patientName;
    
    
    private String contactNumber;
    
    
    private String description;
    
    
    private Long doctorId;
    
    private Employee employee;
    
    
    private Timestamp createdAt;
    
    private Timestamp updatedAt;
    
    public Appointment() {
        //Empty constructor for JPA
    }
    
    public Appointment(Long id) {
        this.id = id;
    }

    public Appointment(Long id, 
            LocalDate date, 
            LocalDateTime startTime, 
            LocalDateTime endTime, 
            Long patientId, 
            Patient patient, 
            String patientName, 
            String contactNumber, 
            String description, 
            Long doctorId, 
            Employee employee) {
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.patientId = patientId;
        this.patient = patient;
        this.patientName = patientName;
        this.contactNumber = contactNumber;
        this.description = description;
        this.doctorId = doctorId;
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Long getPatientId() {
        return patientId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patientId = patient.getId();
        this.patient = patient;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.doctorId = employee.getId();
        this.employee = employee;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Appointment other = (Appointment) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Appointment{" + "id=" + id + ", date=" + date + ", startTime=" + startTime + ", endTime=" + endTime + ", patientId=" + patientId + ", patient=" + patient + ", patientName=" + patientName + ", contactNumber=" + contactNumber + ", description=" + description + ", doctorId=" + doctorId + ", employee=" + employee + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + '}';
    }
    
    
    
    
}
