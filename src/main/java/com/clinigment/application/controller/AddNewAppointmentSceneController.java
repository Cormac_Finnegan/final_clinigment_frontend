

package com.clinigment.application.controller;

import com.clinigment.application.abstracts.LayoutController;
import com.clinigment.application.navigator.LayoutContentNavigator;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.ColorAdjust;
import javafx.stage.Popup;
import javafx.stage.StageStyle;
import javafx.util.Duration;




public class AddNewAppointmentSceneController extends LayoutController implements Initializable {
     // Hold instance of this class
     private static AddNewAppointmentSceneController instance;
     // reference xml objects in code aka instance variables
     @FXML private DatePicker appointmentDate;
     @FXML private ComboBox appointmentStartTime;
     @FXML private ComboBox appointmentEndTime;
     @FXML private CheckBox existingPatientCheckBox;
     @FXML private TextField searchField;
     @FXML private TextField firstNameField;
     @FXML private TextField lastNameField;
     @FXML private TextField contactNumberField;
     @FXML private TextField treatmentField;
     @FXML private Button nextButton;
     @FXML private Button clearButton;
     //hold static times for bookings
     ObservableList<String> list; 
     // used to check the data is valid by value and disable next button if one is set to false 
     Popup calenderMessage;
     private boolean appointmentDateOk; 
     private boolean appointmentStartTimeOk;
     private boolean appointmentEndTimeOk; 
     private boolean existingPatientCheckBoxOK; 
     private boolean searchFieldOk; 
     private boolean firstNameFieldOk; 
     private boolean lastNameFieldOk; 
     private boolean contactNumberFieldOk; 
     private boolean treatmentFieldOk; 
     Alert alert ;
     LocalDate date = LocalDate.now();

    // get the instance of this class or create a new one if no one exists
    public static AddNewAppointmentSceneController getInstance() {
        if(instance == null) {
            instance = new AddNewAppointmentSceneController();
        }
        return instance;
    }
    
    
       public AddNewAppointmentSceneController() {
        super();
    } 
    @Override
    public void setLayout(Node node) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLayout(Node node, Pane container) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
         //instance = this;
        // set the combo box times and assign the values to them
        list = FXCollections.observableArrayList("8:00","8:30","9:00","9:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30"
        ,"14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","20:30","21:00");
        appointmentStartTime.setItems(list);
        appointmentEndTime.setItems(list);
        alert = new Alert(AlertType.INFORMATION);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle(null);
        alert.setHeaderText("Invalid Date");           
        alert.setContentText("You cannot choose a date from the past");
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(
        getClass().getResource("/com/clinigment/application/view/metro/metrostyle/Metro-UI.css").toExternalForm());
        dialogPane.getStyleClass().add("myDialog");
        appointmentStartTime.getStylesheets().add(getClass().getResource("/com/clinigment/application/view/metro/metrostyle/Metro-UI.css").toExternalForm());
        
        // set today's date
        appointmentDate.setValue(LocalDate.now());
        ChangeListener<Object> listener = new FormElementChangeStateListener();
            appointmentStartTime.valueProperty().addListener(listener);
            appointmentEndTime.valueProperty().addListener(listener);
            firstNameField.textProperty().addListener(listener);
            lastNameField.textProperty().addListener(listener);
            appointmentDate.valueProperty().addListener(listener);
            searchField.textProperty().addListener(listener);
            contactNumberField.textProperty().addListener(listener);
            treatmentField.textProperty().addListener(listener);
            existingPatientCheckBox.selectedProperty().addListener(listener);
        
            
        ColorAdjust colorAdjust = new ColorAdjust();
            colorAdjust.setBrightness(0.0);

            nextButton.setEffect(colorAdjust);

            nextButton.setOnMouseEntered(e -> {

                Timeline fadeInTimeline = new Timeline(
                        new KeyFrame(Duration.seconds(0), 
                                new KeyValue(colorAdjust.brightnessProperty(), colorAdjust.brightnessProperty().getValue(), Interpolator.LINEAR)), 
                                new KeyFrame(Duration.seconds(.60), new KeyValue(colorAdjust.brightnessProperty(), -.35, Interpolator.LINEAR)
                                ));
                fadeInTimeline.setCycleCount(1);
                fadeInTimeline.setAutoReverse(false);
                fadeInTimeline.play();

            });

            nextButton.setOnMouseExited(e -> {

                Timeline fadeOutTimeline = new Timeline(
                        new KeyFrame(Duration.seconds(0), 
                                new KeyValue(colorAdjust.brightnessProperty(), colorAdjust.brightnessProperty().getValue(), Interpolator.LINEAR)), 
                                new KeyFrame(Duration.seconds(.60), new KeyValue(colorAdjust.brightnessProperty(), 0, Interpolator.LINEAR)
                                ));
                fadeOutTimeline.setCycleCount(1);
                fadeOutTimeline.setAutoReverse(false);
                fadeOutTimeline.play();

            });
            
            ColorAdjust colorAdjust1 = new ColorAdjust();
            colorAdjust1.setBrightness(0.0);

            clearButton.setEffect(colorAdjust1);

            clearButton.setOnMouseEntered(e -> {

                Timeline fadeInTimeline = new Timeline(
                        new KeyFrame(Duration.seconds(0), 
                                new KeyValue(colorAdjust1.brightnessProperty(), colorAdjust1.brightnessProperty().getValue(), Interpolator.LINEAR)), 
                                new KeyFrame(Duration.seconds(.60), new KeyValue(colorAdjust1.brightnessProperty(), -.35, Interpolator.LINEAR)
                                ));
                fadeInTimeline.setCycleCount(1);
                fadeInTimeline.setAutoReverse(false);
                fadeInTimeline.play();

            });

            clearButton.setOnMouseExited(e -> {

                Timeline fadeOutTimeline = new Timeline(
                        new KeyFrame(Duration.seconds(0), 
                                new KeyValue(colorAdjust1.brightnessProperty(), colorAdjust1.brightnessProperty().getValue(), Interpolator.LINEAR)), 
                                new KeyFrame(Duration.seconds(.60), new KeyValue(colorAdjust1.brightnessProperty(), 0, Interpolator.LINEAR)
                                ));
                fadeOutTimeline.setCycleCount(1);
                fadeOutTimeline.setAutoReverse(false);
                fadeOutTimeline.play();

            });
    }  
    
    // clear the form of all data entered
    public void clearForm(){
        appointmentStartTime.setValue(null);
        appointmentEndTime.setValue(null);
        searchField.clear();          
        firstNameField.clear();           
        lastNameField.clear();          
        contactNumberField.clear();       
        treatmentField.clear(); 
        appointmentDate.setValue(LocalDate.now());
        existingPatientCheckBox.setSelected(false);
        }
    // pull the data from the fields # might be a private method
    public void getDataFromForm(){
        String appointmentStartTimeData = (String) appointmentStartTime.getSelectionModel().getSelectedItem();
        String appointmentEndTimeData = (String) appointmentEndTime.getSelectionModel().getSelectedItem();
        LocalDate date = appointmentDate.getValue();       
        String appointmentDateData = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
        
       // String searchFieldData = searchField.getText().trim();
        String firstNameFieldData = firstNameField.getText().trim();
        String lastNameFieldData = lastNameField.getText().trim();
        String contactNumberFieldData = contactNumberField.getText().trim();
        String treatmentFieldData = treatmentField.getText().trim();
    
    }
    
     private class FormElementChangeStateListener implements ChangeListener<Object> {
         
        @Override
        public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue)
        {
                
              if(appointmentDate.getValue().isAfter(date.minusDays(1)))
               {    
                    appointmentDate.setStyle("-fx-border-color:deepskyblue; -fx-background-color: white;");
                    appointmentDateOk = true;  
               }  
             
               else
               {    
                    appointmentDate.setStyle("-fx-border-color: coral; -fx-background-color: lightsalmon;");
                    appointmentDate.setTooltip(new Tooltip("Date of Appointment cannot be in the past"));
                    appointmentDateOk = false;
                    appointmentDate.setValue(LocalDate.now()); // Trying to stop nasty exception
                    alert.showAndWait(); // error dialog
               }
                    
              if(appointmentStartTime.getSelectionModel().getSelectedIndex() >= 0)
              { 
                appointmentStartTimeOk = true;  
              }  
              else
              {
                appointmentStartTimeOk = false;
                appointmentStartTime.setTooltip(new Tooltip("Select a Appointment Start Time"));
              }
           
               if(appointmentEndTime.getSelectionModel().getSelectedIndex() >= 0)
              { 
                appointmentEndTimeOk = true;  
              }  
              else
              {
                appointmentEndTimeOk = false;
                appointmentEndTime.setTooltip(new Tooltip("Select a Appointment End Time"));
              }       
             
            
               if(existingPatientCheckBox.isSelected()) 
               {
                   existingPatientCheckBoxOK = true;         
               }
               else
               {
                    existingPatientCheckBoxOK = false;
               } 
             
            
                 //---------------------------------------------------------------------------------
                //Check for valid name allow spaces,hyphens and apostrophe - is it a letter? Spaces?
             if(firstNameField.getText().trim().matches("[A-Za-z -']*")  &&
                firstNameField.getText().trim().length() != 0
               )
             {  // remove any visual error indicator,clear tooltip error and set boolean check to true
                firstNameField.setStyle("-fx-border-color:deepskyblue; -fx-background-color: white;");
                firstNameField.setTooltip(null);
                firstNameFieldOk = true;
                        
             }  
             
             else if(firstNameField.getText().trim().length() == 0)
             {  // remove any visual error indicator,add tooltip and set boolean check to true
                firstNameFieldOk = true; 
                firstNameField.setStyle("-fx-border-color:deepskyblue; -fx-background-color: white;");
                firstNameField.setTooltip(new Tooltip("Enter First Name"));
             }
              else 
              { // style the text first name textbox,add error tooltip and set boolean check to false   
                firstNameField.setStyle("-fx-border-color: coral; -fx-background-color: lightsalmon;");
                firstNameField.setTooltip(new Tooltip("Alfabete Characters,Hyphens and Apostrophes Only"));
                firstNameFieldOk = false;
              }
             
             
             
                         //---------------------------------------------------------------------------------
                //Check for valid name allow spaces,hyphens and apostrophe - is it a letter? Spaces?
             if(lastNameField.getText().trim().matches("[A-Za-z -']*")  &&
                lastNameField.getText().trim().length() != 0
               )
             {  // remove any visual error indicator,clear tooltip error and set boolean check to true
                lastNameField.setStyle("-fx-border-color:deepskyblue; -fx-background-color: white;");
                lastNameField.setTooltip(null);
                lastNameFieldOk = true;
                //validationSupport.registerValidator(nextOfKinNameField,Validator.createEmptyValidator("Last Name Cannot Be left Empty"));            
             }  
             
             else if(lastNameField.getText().trim().length() == 0)
             {  // remove any visual error indicator,add tooltip and set boolean check to true
                lastNameFieldOk = true; 
                lastNameField.setStyle("-fx-border-color:deepskyblue; -fx-background-color: white;");
                lastNameField.setTooltip(new Tooltip("Enter Surname"));
             }
              else 
              { // style the text first name textbox,add error tooltip and set boolean check to false   
                lastNameField.setStyle("-fx-border-color: coral; -fx-background-color: lightsalmon;");
                lastNameField.setTooltip(new Tooltip("Alfabete Characters,Hyphens and Apostrophes Only"));
                lastNameFieldOk = false;
              }
             
                     //---------------------------------------------------------------------------------
                //Check for valid number will allow + and international lengths
             if(contactNumberField.getText().trim().matches("[0-9+]*") &&
                contactNumberField.getText().trim().length() >= 7 &&  
                contactNumberField.getText().trim().length() <= 14 &&
                contactNumberField.getText().trim().length() != 0
               )
             {   // remove any visual error indicator,clear tooltip error and set boolean check to true
                contactNumberField.setStyle("-fx-border-color:deepskyblue; -fx-background-color: white;");
                contactNumberField.setTooltip(null);
                contactNumberFieldOk = true;
                      
             }  
              else if(contactNumberField.getText().trim().length() == 0)
              {  // remove any visual error indicator,add tooltip and set boolean check to true
                contactNumberFieldOk = false; 
                contactNumberField.setStyle("-fx-border-color:deepskyblue; -fx-background-color: white;");
                contactNumberField.setTooltip(new Tooltip("Enter Contact Number"));
              }
              else
              { // style the text first name textbox,add error tooltip and set boolean check to false   
                contactNumberField.setStyle("-fx-border-color: coral; -fx-background-color: lightsalmon;");
                contactNumberField.setTooltip(new Tooltip("Numbers Only Allowed Between 7 and 14 Digits in Length"));
                contactNumberFieldOk = false;
              }
             
                      
             // check if all fields of data are correct and valid
           if( appointmentDateOk  && appointmentStartTimeOk && appointmentEndTimeOk && existingPatientCheckBoxOK && searchFieldOk && firstNameFieldOk && lastNameFieldOk 
              &&contactNumberFieldOk &&treatmentFieldOk
             ) 
           
            {    // make next button clickable  
                nextButton.setDisable(false);
            } 
            else 
            {
                //make next button unclickable
                nextButton.setDisable(false);
            }
        }
        
    } 
    
}
