
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clinigment.application.controller;


import com.clinigment.application.abstracts.LayoutController;
import com.clinigment.application.main.App;
import com.clinigment.application.model.Patient;
import com.clinigment.application.view.metro.animations.FadeInUpTransition;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.StageStyle;
import javafx.util.Callback;

/**
 *
 * @author csaba
 */
public class AppointmentTableSceneController extends LayoutController implements Initializable {

    @FXML
    private AnchorPane tablePane, container, patientInfoPane;
   
    @FXML
    private StackPane stackPane;
    
    @FXML
    private TableView tableView;
    
    @FXML
    private Button addNewPatientButton;
    
    @FXML
    private TableColumn colAction;
    @FXML
    private TableColumn<Patient, String> colPatientId,
                                         colFirstName,
                                         colLastName,
                                         colPpsNumber,
                                         colDateOfBirth,
                                         colGender,
                                         colBloodType,
                                         colMobilePhone,
                                         colHomePhone,
                                         colEmail;
    
    @Override
    public void setLayout(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLayout(Node node, Pane container) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
       // colPatientId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        colLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        colPpsNumber.setCellValueFactory(new PropertyValueFactory<>("ppsNumber"));
        colDateOfBirth.setCellValueFactory(new PropertyValueFactory<>("dateOfBirth"));
        //colGender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        //colBloodType.setCellValueFactory(new PropertyValueFactory<>("bloodType"));
        colMobilePhone.setCellValueFactory(new PropertyValueFactory<>("mobilePhone"));
        colHomePhone.setCellValueFactory(new PropertyValueFactory<>("homePhone"));
        colEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        
        //Set items
        tableView.setItems(getPatients());
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        colAction.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Object, Boolean>,ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Object, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            colAction.setCellFactory(new Callback<TableColumn<Object, Boolean>, TableCell<Object, Boolean>>() {
                @Override
                public TableCell<Object, Boolean> call(TableColumn<Object, Boolean> p) {
                    return new ActionTableCell(tableView);
                }
            });
            
        addNewPatientButton.setOnAction(e -> {
            System.out.println("Clicked");
            MainLayoutController.getInstance().setActive(MainLayoutController.SCENES.get(MainLayoutController.NEW_PATIENT_PANE_INDEX));
        });
        
    }
    
    @FXML
    public void savePatient() {}
    
    @FXML
    public void goBack() {}
    
    @FXML
    public void addNewPatient() {}
    
    @FXML
    public void showAppointmentData(MouseEvent event) {}
    
    public ObservableList<Patient> getPatients() {
        //ObservableList<Patient> patients = FXCollections.observableArrayList();
        ObservableList<Patient> patientList = FXCollections.observableArrayList();
        return patientList;
    }
    
    private class ActionTableCell extends TableCell<Object, Boolean> {
        
        final Hyperlink viewButton = new Hyperlink("View");
        final Hyperlink editButton = new Hyperlink("Edit");
        final HBox hb = new HBox(viewButton, editButton);
        
        ActionTableCell (final TableView tblView){
            hb.setSpacing(4);
            viewButton.setOnAction((ActionEvent t) -> {
                //status = 1;
                int row = getTableRow().getIndex();
                tableView.getSelectionModel().select(row);
                showAppointmentData(null);
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete the selected appointment?");
                alert.initStyle(StageStyle.UTILITY);
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    Patient p = new Patient();
                    //p.setId(Integer.valueOf(txtId.getText()));
                    //crud.delete(p);
                    //selectData();
                }else{
                    //selectData();
                    //auto();
                }
                //status = 0;
            });
            
            editButton.setOnAction((ActionEvent event) -> {
                //status = 1;
                int row = getTableRow().getIndex();
                tableView.getSelectionModel().select(row);
                //showPatientData(null);
//                tablePane.setOpacity(0);
                //new FadeInUpTransition(patientInfoPane).play();
                //status = 0;
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(hb);
            }else{
                setGraphic(null);
            }
        }
    }
}




