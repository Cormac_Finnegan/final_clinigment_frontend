
package com.clinigment.application.controller;

import com.clinigment.application.abstracts.LayoutController;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.clinigment.application.main.App;
import com.clinigment.application.model.Employee;
import com.clinigment.application.model.EmployeeAddress;
import com.clinigment.application.model.Patient;
import com.clinigment.application.model.PatientAddress;
import com.clinigment.model.enums.Gender;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.StageStyle;
import javafx.util.Duration;


public class AddNewEmployeeSceneContoller extends LayoutController implements Initializable {
    // Hold instance of this class
    private static AddNewEmployeeSceneContoller instance;
   
    // reference xml objects in code aka instance variables
     @FXML private DatePicker dateOfBirthComboBox;
     @FXML private TextField firstNameField;
     @FXML private TextField middleNameField;
     @FXML private TextField lastNameField;
     @FXML private TextField ppsNumberField;
     @FXML private Button nextButton;
     @FXML private Button clearButton;
     @FXML private TextField mobileField;
     @FXML private TextField homePhoneField;
     @FXML private TextField emailField;
     @FXML private TextField addressLine1Field;
     @FXML private TextField addressLine2Field;
     @FXML private TextField addressLine3Field;
     @FXML private TextField cityTownField;
     @FXML private TextField countyField;
     @FXML private TextField countryField;
     // used to check the data is valid by value and disable next button if one is set to false 
     Popup calenderMessage;
     Alert alert;
     LocalDate date = LocalDate.now();
     
     private boolean dateOfBirthComboBoxOk; 
     private boolean firstNameFieldOk;
     private boolean middleNameFieldOk; 
     private boolean lastNameFieldOK; 
     private boolean ppsNumberFieldOk; 
     
     private boolean mobileFieldOk; 
     private boolean homePhoneFieldOk;
     private boolean emailFieldOk; 
     private boolean addressLine1FieldOK; 
     private boolean addressLine2FieldOk; 
     
     private boolean addressLine3FieldOk; 
     private boolean cityTownFieldOk;
     private boolean countyFieldOk; 
     private boolean countryFieldOK;

     
        // get the instance of this class or create a new one if no one exists
    public static AddNewEmployeeSceneContoller getInstance() {
        if(instance == null) {
            instance = new AddNewEmployeeSceneContoller();
        }
        return instance;
    }
    
    
       public AddNewEmployeeSceneContoller() {
        super();
    }  
     
    @Override
    public void setLayout(Node node) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLayout(Node node, Pane container) {
     //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
      alert = new Alert(Alert.AlertType.INFORMATION);
      alert.initStyle(StageStyle.UTILITY);
      alert.setTitle(null);
      alert.setHeaderText("Invalid Date");           
      alert.setContentText("You cannot choose a date from the Future");
      DialogPane dialogPane = alert.getDialogPane();
      dialogPane.getStylesheets().add(
      getClass().getResource("/com/clinigment/application/view/metro/metrostyle/Metro-UI.css").toExternalForm());
      dialogPane.getStyleClass().add("myDialog");
      
      
        dateOfBirthComboBox.setValue(LocalDate.now());
  /*      ChangeListener<Object> listener = new FormElementChangeStateListener();
            appointmentStartTime.valueProperty().addListener(listener);
            appointmentEndTime.valueProperty().addListener(listener);
            firstNameField.textProperty().addListener(listener);
            lastNameField.textProperty().addListener(listener);
            middleNameField.textProperty().addListener(listener);
            ppsNumberField.textProperty().addListener(listener);
            ppsNumberField.textProperty().addListener(listener);       
        
            contactNumberField.textProperty().addListener(listener);
            treatmentField.textProperty().addListener(listener);
            existingPatientCheckBox.selectedProperty().addListener(listener);
     */ 
        ColorAdjust colorAdjust = new ColorAdjust();
            colorAdjust.setBrightness(0.0);

            nextButton.setEffect(colorAdjust);

            nextButton.setOnMouseEntered(e -> {

                Timeline fadeInTimeline = new Timeline(
                        new KeyFrame(Duration.seconds(0), 
                                new KeyValue(colorAdjust.brightnessProperty(), colorAdjust.brightnessProperty().getValue(), Interpolator.LINEAR)), 
                                new KeyFrame(Duration.seconds(.60), new KeyValue(colorAdjust.brightnessProperty(), -.35, Interpolator.LINEAR)
                                ));
                fadeInTimeline.setCycleCount(1);
                fadeInTimeline.setAutoReverse(false);
                fadeInTimeline.play();

            });

            nextButton.setOnMouseExited(e -> {

                Timeline fadeOutTimeline = new Timeline(
                        new KeyFrame(Duration.seconds(0), 
                                new KeyValue(colorAdjust.brightnessProperty(), colorAdjust.brightnessProperty().getValue(), Interpolator.LINEAR)), 
                                new KeyFrame(Duration.seconds(.60), new KeyValue(colorAdjust.brightnessProperty(), 0, Interpolator.LINEAR)
                                ));
                fadeOutTimeline.setCycleCount(1);
                fadeOutTimeline.setAutoReverse(false);
                fadeOutTimeline.play();

            });
            
            ColorAdjust colorAdjust1 = new ColorAdjust();
            colorAdjust1.setBrightness(0.0);

            clearButton.setEffect(colorAdjust1);

            clearButton.setOnMouseEntered(e -> {

                Timeline fadeInTimeline = new Timeline(
                        new KeyFrame(Duration.seconds(0), 
                                new KeyValue(colorAdjust1.brightnessProperty(), colorAdjust1.brightnessProperty().getValue(), Interpolator.LINEAR)), 
                                new KeyFrame(Duration.seconds(.60), new KeyValue(colorAdjust1.brightnessProperty(), -.35, Interpolator.LINEAR)
                                ));
                fadeInTimeline.setCycleCount(1);
                fadeInTimeline.setAutoReverse(false);
                fadeInTimeline.play();

            });

            clearButton.setOnMouseExited(e -> {

                Timeline fadeOutTimeline = new Timeline(
                        new KeyFrame(Duration.seconds(0), 
                                new KeyValue(colorAdjust1.brightnessProperty(), colorAdjust1.brightnessProperty().getValue(), Interpolator.LINEAR)), 
                                new KeyFrame(Duration.seconds(.60), new KeyValue(colorAdjust1.brightnessProperty(), 0, Interpolator.LINEAR)
                                ));
                fadeOutTimeline.setCycleCount(1);
                fadeOutTimeline.setAutoReverse(false);
                fadeOutTimeline.play();

            });
    }
     
     
      public void clearForm(){
        
        ppsNumberField.clear();          
        firstNameField.clear();           
        lastNameField.clear();          
        middleNameField.clear();       
        mobileField.clear(); 
        dateOfBirthComboBox.setValue(LocalDate.now());
        homePhoneField.clear();          
        emailField.clear();  
        addressLine1Field.clear();          
        addressLine2Field.clear();  
        addressLine3Field.clear();          
        cityTownField.clear();  
        countyField.clear();          
        countryField.clear();   
  
        }

    /*****NEW PATIENT VARIABLE*****/
    private Employee newEmployee;


    @FXML
    public Boolean loadConfirmScene() {
        //Parse data from fields
        //Name

        //System.out.println("From Add New patient"+this);
        //String title = (String) titleComboBox.getSelectionModel().getSelectedItem();
        //System.out.println(titleComboBox);
        String firstName = firstNameField.getText().trim();
        String lastName = lastNameField.getText().trim();
        String middleName = middleNameField.getText().trim();



        LocalDate date = null;
        String errorMessage = "";


        //Date of birth
        try {
            date = dateOfBirthComboBox.getValue();
            String dob = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
        }catch (NullPointerException n){
            errorMessage += "Please provide a Date of Birth\n";
        }

        //PPS Number
        String ppsNumber = ppsNumberField.getText().trim();


        //Address
        String addressLine1 = addressLine1Field.getText().trim();
        String addressLine2 = addressLine2Field.getText().trim();
        String addressLine3 = addressLine3Field.getText().trim();
        String city = cityTownField.getText().trim();
        String country = countryField.getText().trim();
        String county = countyField.getText().trim();


        //Phone numbers and email
        String homePhone = homePhoneField.getText().trim();
        String mobilePhone = mobileField.getText().trim();
        String email = emailField.getText().trim();
        //String genString = (String) gen.getSelectionModel().getSelectedItem();
        /*
        Gender gender;
        if(genString.equals(Gender.MALE.toString())){
            gender = Gender.MALE;
        }else if(genString.equals(Gender.FEMALE.toString())){
            gender = Gender.FEMALE;
        }else{
            gender = Gender.OTHER;
        }
        */

        Boolean error = false;


        if(firstName == null || firstName.equals("")){
            errorMessage += "Please provide a First Name\n";
            error = true;
        }
        if(lastName == null || lastName.equals("")){
            errorMessage += "Please provide a Surname\n";
            error = true;
        }
        if(ppsNumber == null || ppsNumber.equals("")){
            errorMessage += "Please provide a PPS Number\n";
            error = true;
        }
        if(mobilePhone == null || mobilePhone.equals("")){
            errorMessage += "Please provide a valid Phone Nummber\n";
            error = true;
        }
        else{
            Pattern pattern = Pattern.compile("\\d{10}");
            Matcher matcher = pattern.matcher(mobilePhone);
            if (matcher.matches()) {
                System.out.println("Phone Number Valid");
            }
            else
            {
                errorMessage += "Please provide a valid Phone Nummber\n";
                error = true;
            }
        }
        if(firstName == null || firstName == ""){
            errorMessage += "Please provide a First Name\n";
            error = true;
        }


        if(error == true){
            newAlert(errorMessage);
        }else {
            Employee SWAP = new Employee();
            //SWAP.setTitle(title);
            SWAP.setFirstName(firstName);
            SWAP.setMiddleName(middleName);
            SWAP.setLastName(lastName);
            SWAP.setDateOfBirth(date);
            //SWAP.setGender(gender);
            SWAP.setPpsNumber(ppsNumber);

            SWAP.setHomePhone(homePhone);
            SWAP.setMobilePhone(mobilePhone);
            SWAP.setEmail(email);

            EmployeeAddress SWAPAddress = new EmployeeAddress();
            SWAPAddress.setAddressLine1(addressLine1);
            SWAPAddress.setAddressLine2(addressLine2);
            SWAPAddress.setAddressLine3(addressLine3);
            SWAPAddress.setCityTown(city);
            SWAPAddress.setCountry(country);
            SWAPAddress.setCounty(county);
            SWAP.setEmployeeAddress(SWAPAddress);




            this.newEmployee = SWAP;
            System.out.println("New Patient: " + this.newEmployee);
            SWAP = null;
        }

        return error;
    }



    private void newAlert(String message){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message);
        alert.initStyle(StageStyle.UTILITY);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            //Patient p = new Patient();
            //p.setId(Integer.valueOf(txtId.getText()));
            //crud.delete(p);
            //selectData();
        }else{
            //selectData();
            //auto();
            //loadConfirmScene();
            loadConfirmScene();
        }
    }

    private String allergyCompositeString;

    @FXML
    public void goToNextScene() {
        if(!loadConfirmScene()) {
            MainLayoutController.getInstance().setActive(MainLayoutController.SCENES.get(MainLayoutController.VIEW_NEW_PATIENT_PANE_INDEX_5));

            //System.out.println(ViewPatientDetailsSingleController.getInstance());
            ViewPatientDetailsController controller = ViewPatientDetailsController.getInstance();
            //System.out.println("Controller from second instantiation"+ViewPatientDetailsController.getInstance());
            //controller.putData(this.newEmployee);
        }else{
            //loadConfirmScene();

        }


    }

    @FXML
    public void goToViewScene() {/*
        MainLayoutController.getInstance().setActive(
                MainLayoutController.SCENES.get(
                        MainLayoutController.VIEW_NEW_PATIENT_PANE_INDEX_5
                )
        );
        */
        loadConfirmScene();

    }

    @FXML
    public void goBack() {
        MainLayoutController.getInstance().setActive(
                MainLayoutController.SCENES.get(
                        MainLayoutController.NEW_PATIENT_PANE_INDEX
                )
        );
    }



  
}
