/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clinigment.application.controller;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;
import javax.ws.rs.core.GenericType;

import com.clinigment.application.model.Patient;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;

/**
 *
 * @author csaba
 */
public class PatientClient {

    public static void main(String[] args){
        PatientClient test = new PatientClient();
    }


    List<Patient> patientList;
    public PatientClient(){
        Client client = this.getClient();
        patientList = getAllPatient(client);
    }
    
    public Client getClient(){
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "admin");
        
        ClientConfig cc = new ClientConfig().register(new JacksonFeature());
        cc.register(feature);
        
        Client client = ClientBuilder.newClient(cc);
        return client;
    }
    
    public List getAllPatient(Client client){
        WebTarget allPatientsTarget = client
                .target("http://ec2-52-19-236-212.eu-west-1.compute.amazonaws.com/webapi/patients");
        
        Response allPatientsResponse = allPatientsTarget.request(MediaType.APPLICATION_JSON).get();
        List<Patient> patients = null;
        switch (allPatientsResponse.getStatus()) {
            case 200:
                patients = allPatientsResponse.readEntity(new GenericType<List<Patient>>(){});
                System.out.println("Patients: " + patients);
                break;
            case 404:
                System.out.println("Patient was not found.");
                break;
            case 204:
                System.out.println("Patient was not found.");
                break;
            default:
                System.out.println("An error occured.");
                break;
        }
        allPatientsResponse.close();
        return patients;
    }
    
    public List<Patient> getpatientList(){
        return patientList;
    }
    
}