/*
Author:
Last Changed: 17/03/2016
Purpose:
*/
package com.clinigment.application.controller;

import com.clinigment.application.abstracts.LayoutController;
import com.clinigment.application.navigator.LayoutContentNavigator;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
//import org.clingiment.backend.api.LoginEntity;
//import org.clingiment.backend.api.LoginEntity;

public class LoginController extends LayoutController implements Initializable {
	
        private static LoginController instance;
        
        public static LoginController getInstance() {
            if(instance == null) {
                instance = new LoginController();
            }
            return instance;
        }
        
        public LoginController() {
            super();
        }
        
	@FXML private TextField usernameField;
	@FXML private PasswordField passwordField;
	@FXML private Button button;
	@FXML private Text confirmText;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
            instance = this;
            button.setDefaultButton(true);
	}
	
	@FXML
	public void validateLoginDetails() throws IOException {
            if(true) {
               LayoutContentNavigator.loadLayout(LayoutContentNavigator.MAIN_LAYOUT);
            } else {

            //LoginEntity loginEntity = new LoginEntity();
            //loginEntity.setUsername(usernameField.getText());
            //loginEntity.setPassword(passwordField.getText());

            //UserClient userClient = new UserClient();
            //loginEntity = userClient.login_JSON(loginEntity, LoginEntity.class);
            //System.out.println(loginEntity.getMessage());

            if(true/*loginEntity.getMessage().equals("Login succesful")*/) {
               LayoutContentNavigator.loadLayout(LayoutContentNavigator.MAIN_LAYOUT);
               //userClient.close();
            } else {
                //confirmText.setText(loginEntity.getMessage());

                usernameField.clear();
                passwordField.clear();
                usernameField.requestFocus();
            }       
	}

        }
    @Override
    public void setLayout(Node node) {
       
    }

   
    @Override
    public void setLayout(Node node, Pane container) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

